# Design a system that compares multiple companies’ environmental pollution

## Initial purpose : **	Design a system that compares multiple companies’ environmental pollution **
## Considerations:

* What type of pollution is this measuring? **Carbon emissions**
* Area restrictions? To a city, a county, country, area of the world? **UK**
* Unit of measurement : **CO2**
* Type of company? **Car companies (Car manufacturers, testing...) / All car companies or top 10/20 UK car companies. International car companies based in uk?**
* Type of test? **Real world testing over a certain period of time? Specific environment for testing?**

## Starting point?

* List of car companies for program to read through
* List of testing results in CO2 as a unit of measurement
* Ranking system? 
* Input two or more companies to specifically compare?

## Top 6 car companies in UK

* Ford
* Volkswagen
* Vauxhall	
* Mercedes-Benz
* Audi
* BMW